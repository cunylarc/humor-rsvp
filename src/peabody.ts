export var practice = [
    {"filename": "ball.jpg"},
    {"filename": "bathtub.jpg"},
    {"filename": "caterpillar.jpg"},
    {"filename": "flower.jpg"},
    {"filename": "glass.jpg"},
    {"filename": "hair.jpg"},
    {"filename": "queen.jpg"},
    {"filename": "sock.jpg"},
    {"filename": "train.jpg"}
];

export var experimental = [
    {"category": "animal", "filename": "squirrel.jpg"},
    {"category": "tool", "filename": "lamp.jpg"},
    {"category": "misc", "filename": "heart.jpg"},
    {"category": "person", "filename": "clown.jpg"},
    {"category": "animal", "filename": "raccoon.jpg"},
    {"category": "insect", "filename": "fly.jpg"},
    {"category": "tool", "filename": "comb.jpg"},
    {"category": "nature", "filename": "mountain.jpg"},
    {"category": "animal", "filename": "kangaroo.jpg"},
    {"category": "vehicle", "filename": "rocket.jpg"},
    {"category": "nature", "filename": "moon.jpg"},
    {"category": "clothing", "filename": "necklace.jpg"},
    {"category": "building", "filename": "castle.jpg"},
    {"category": "tool", "filename": "spoon.jpg"},
    {"category": "vehicle", "filename": "airplane.jpg"},
    {"category": "furniture", "filename": "clock.jpg"},
    {"category": "instrument", "filename": "guitar.jpg"},
    {"category": "toy", "filename": "robot.jpg"},
    {"category": "fruit\/veg", "filename": "carrot.jpg"},
    {"category": "body", "filename": "beard.jpg"},
    {"category": "tool", "filename": "skis.jpg"},
    {"category": "instrument", "filename": "harp.jpg"},
    {"category": "animal", "filename": "crab.jpg"},
    {"category": "clothing", "filename": "glasses.jpg"},
    {"category": "furniture", "filename": "vase.jpg"},
    {"category": "animal", "filename": "horse.jpg"},
    {"category": "instrument", "filename": "violin.jpg"},
    {"category": "tool", "filename": "hammer.jpg"},
    {"category": "insect", "filename": "spider.jpg"},
    {"category": "clothing", "filename": "helmet.jpg"},
    {"category": "fruit\/veg", "filename": "asparagus.jpg"},
    {"category": "animal", "filename": "rabbit.jpg"},
    {"category": "insect", "filename": "snail.jpg"},
    {"category": "tool", "filename": "kettle.jpg"},
    {"category": "clothing", "filename": "crown.jpg"},
    {"category": "person", "filename": "soldier.jpg"}
];
