import { delay, getKey, preloadImages } from './utils';
import { utils } from 'larc-exp';
import { runTimeline } from 'larc-exp';
import { jokes } from './jokes';
import * as pictures from './peabody';
import { shuffle } from './utils';

function sentences(lines) {
    return lines.map(
        line => ({
            text: line
                .split(/\n/)
                .map(x => `<p>${x}</p>`)
                .join('')
        }));
}

function randomizedConditions(count) {
    let conditions = ['joke', 'nonJoke'];
    shuffle(conditions);
    let condBlock = new Array(Math.floor(count / 2))
        .fill(conditions[0])
        .concat(new Array(Math.ceil(count / 2))
            .fill(conditions[1]));
    shuffle(condBlock);
    return condBlock;
}

function addConditions(jokeList) {
    let conditions = randomizedConditions(jokeList.length);
    conditions.forEach((condition, index) => {
        jokeList[index]['condition'] = condition;
    });
}

[jokes.practice, jokes.experimental].forEach(jokeList => {
    addConditions(jokeList);
    shuffle(jokeList);
});

preloadImages('images/peabody',
    pictures.practice
        .concat(pictures.experimental)
        .map(({filename}) => filename));

let RSVP_DELAY = 250  // 4 words per second
let [notFunnyKey, funnyKey] = ['q', 'p']; // can only be alphanumeric.
// otherwise, need to use keypress
// events or refactor how key capture
// is handled.

const rsvpInterInstructions = `
<p>Press space to see the next phrase</p>
<p>Remember to press <big>${funnyKey}</big> for funny and <big>${notFunnyKey}</big> for not funny</p>
`;

let timeline = [

    {
        type: 'instruction',
        advanceKeys: [' '],
        footer: '<small>(press spacebar to continue)</small>',
        timeline: sentences([
            'You will see a series of jokes, one word at a time.',
            `If you think the joke is funny, press the <big>${funnyKey}</big> key.`,
            `If you think the joke isn't funny, press the <big>${notFunnyKey}</big> key.`,
            "Let's try some practice items.",
        ]),
    },

    {
        type: 'rsvp',
        phase: 'rsvp-practice',
        instructions: rsvpInterInstructions,
        delay: RSVP_DELAY,
        responseKeys: [notFunnyKey, funnyKey],
        timeline: jokes.practice
    },

    {
        type: 'instruction',
        advanceKeys: [' '],
        text: `<p>Great. Now let's do the real trials.<p/>`,
        footer: '<small>Press spacebar to begin</small>'
    },

    {
        type: 'rsvp',
        phase: 'rsvp-experimental',
        instructions: rsvpInterInstructions,
        delay: RSVP_DELAY,
        responseKeys: [notFunnyKey, funnyKey],
        timeline: jokes.experimental,
    },

    {
        type: 'qualtrics',
        url: 'https://cunyhunter.co1.qualtrics.com/SE/?SID=SV_bBHM31XKbte91UV'
    },

    {
        type: 'instruction',
        text: 'Thank you for participating!'
    }

];

window.addEventListener('load', () => {
    runTimeline('.content', timeline).then((x) => console.log(x));
});
