
window.addEventListener('load', main);

function values(obj) {
    let vals = [];
    for (let key in obj) {
        vals.push(obj[key]);
    }
    return vals
}

function getRows(snapshot): any {
    return values(snapshot.val()).map(values);
}

function collectKeys(trials) {
    let keys = {};
    trials.forEach((rows) => {
        rows.forEach((row) => {
            for (let key in row) {
                keys[key] = true;
            }
        });
    });
    let keylist = [];
    for (let key in keys) {
        keylist.push(key);
    }
    return keylist;
}

function trialsToCsv(trials) {
    let keys = collectKeys(trials);
    let datalist = [keys.join('\t')];
    trials.forEach((rows) => {
        rows.forEach((row) => {
            let x = [];
            keys.forEach((key) => {
                x.push(row[key]);
            });
            datalist.push(x.join('\t').replace(/\n/g, ' '));
        });
    });
    return datalist;
}

declare global {
    function unescape(s: string): string
}

function downloadCsv(data) {
    var link = document.createElement('a');
    link.download = 'data.tsv';
    link.href = 'data:text/tsv;base64,' + btoa(unescape(encodeURIComponent(data.join('\n'))));
    link.innerHTML = 'Download Experiment Data';
    document.body.appendChild(link);
}

export function main() {
    let database = window['firebase'].database().ref('session');
    database.once('value')
        .then(getRows)
        .then(trialsToCsv)
        .then(downloadCsv)
        .catch((e) => console.log(e));
}
