'use strict';

var path = require('path');
const UglifyJSPlugin = require('uglifyjs-webpack-plugin');

var babelOptions = {
    "presets": [
        [
            "es2015",
            {
                "modules": false
            }
        ],
        "es2016"
    ]
};

module.exports = {
    cache: true,
    entry: {
        app: ['./src/app.ts'],
        download: './src/download.ts',
        vendor: [
            'babel-polyfill',
        ]
    },
    output: {
        path: path.resolve(__dirname, './dist/js'),
        filename: '[name].js',
        chunkFilename: '[chunkhash].js'
    },
    resolve: {
        extensions: ['.ts', '.tsx', '.js']
    },
    plugins: [
        // new UglifyJSPlugin()
    ],
    module: {
        rules: [{
            test: /\.ts(x?)$/,
            exclude: /node_modules/,
            use: [
                {
                    loader: 'babel-loader',
                    options: babelOptions
                },
                {
                    loader: 'ts-loader'
                }
            ]
        }, {
            test: /\.js$/,
            // exclude: /node_modules/,
            use: [
                {
                    loader: 'babel-loader',
                    options: babelOptions
                }
            ]
        }]
    }
};
